{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Main where

import Control.Lens
import Control.Monad (forM_, when, void)
import Control.Monad.IO.Class (MonadIO (..))
import Control.Monad.Trans.State
import Crypto.Hash (hash, Digest, MD5)
import qualified Data.ByteArray as A
import qualified Data.ByteArray.Encoding as A
import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Data.List.Extra (trimEnd, trim, breakOn)
import Data.Maybe
import qualified Data.HashMap.Lazy as H
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import Data.Word (Word8)
import Magic
import Network.AWS
import Network.AWS.Data
import Network.AWS.Data.Body
import Network.AWS.S3
import Path (reldir, Path (..), Rel, Dir, File, (</>), toFilePath)
import qualified Path.IO as P
import System.Directory ( doesFileExist
                        , getHomeDirectory
                        , listDirectory
                        , setCurrentDirectory
                        )
import System.Environment (getArgs, getProgName, lookupEnv)
import System.IO

import Mime (mimeLookup, defaultMimeType)

data SyncCheck = SyncCheck { scCloudETag :: ETag
                           , scLocalETag :: ETag
                           , scLocalMD5  :: ByteString
                           }
               deriving (Show)

type MyHashMap = H.HashMap ObjectKey SyncCheck

defSC :: SyncCheck
defSC = SyncCheck "" "" ""

main :: IO ()
main = do
  args <- getArgs
  prog <- getProgName
  (localDir, bucketName) <-
        case args of
          a1:a2:_ -> return (a1, BucketName $ T.pack a2)
          _       -> fail $ "Number of arguments is less than 2!\n" ++
                            "Usage: " ++ prog ++ " <local dir> <bucket name>"

  -- cloud
  lgr <- newLogger Info stdout
  reg <- readRegionFromFile
  env <- (& envLogger .~ lgr) <$> newEnv Discover >>= \e -> return $
             if e ^. envRegion == NorthVirginia
             then maybe e (\r -> e & envRegion .~ r) reg
             else e
  res <- runResourceT $ runAWS env $
             send (listObjectsV2 bucketName)
  let hm = makeHashMapWith res

  -- local
  setCurrentDirectory localDir
  hm' <- execStateT (P.walkDirRel walkFunc [reldir|.|]) hm

  -- sync
  runResourceT $ runAWS env $
     forM_ (H.toList hm') $ \(k, v) ->
       let keyStr = T.unpack $ toText k
       in do
         magic <- liftIO $ magicOpen [MagicMimeType]
         liftIO $ magicLoadDefault magic
         if scCloudETag v == "" || scLocalETag v /= scCloudETag v
         then do contentType <- liftIO $ guessContentType magic keyStr
                 liftIO $ putStrLn $ "PutObject: " ++
                                     keyStr ++ " type: " ++ show contentType
                 file <- liftIO $ B.readFile keyStr
                 void $ send $
                   putObject bucketName k (toBody file)
                   & poContentMD5 ?~ T.decodeUtf8
                   (A.convertToBase A.Base64 $ scLocalMD5 v)
                   & poContentType .~ contentType
         else when (scLocalETag v == "") $ do
                 liftIO $ putStrLn $ "DeleteObject: " ++ keyStr
                 void $ send (deleteObject bucketName k)

guessContentType :: Magic -> String -> IO (Maybe Text)
guessContentType magic path = let path' = T.pack path
                                  mime1 = mimeLookup path'
  in if mime1 == defaultMimeType
     then do mime2 <- magicFile magic path
             return $ Just (T.pack mime2)
     else return $ Just (T.decodeUtf8 mime1)

walkFunc :: Path Rel Dir -> [Path Rel Dir] -> [Path Rel File]
         -> StateT MyHashMap IO (P.WalkAction Rel)
walkFunc dir subdirs files = do
  forM_ files $ \file -> do
    hmap <- get
    let path      = toFilePath $ dir </> file
        key       = ObjectKey . T.pack $ path
        syncCheck = H.lookup key hmap
    (e, m) <- liftIO $ md5Hash path
    case syncCheck of
      Nothing -> put $ hmap & at key ?~ defSC { scLocalETag = e
                                              , scLocalMD5 = m
                                              }
      Just ck -> put $ hmap & at key ?~ ck { scLocalETag = e
                                           , scLocalMD5 = m
                                           }
  return $ P.WalkExclude []

md5Hash :: FilePath -> IO (ETag, ByteString)
md5Hash path = do
  bs <- B.readFile path
  let md5  = hash bs :: Digest MD5
      eTag = ETag . C.pack $ "\"" ++ show md5 ++ "\""
  return (eTag, B.pack $ A.unpack md5)

readRegionFromFile :: IO (Maybe Region)
readRegionFromFile = do
  confPath   <- (++ "/.aws/config") <$> getHomeDirectory
  confExists <- doesFileExist confPath
  region' <- if confExists
             then lookup "region" <$> readConfFile "default" confPath
             else return Nothing
  return $ (\r -> case fromText . T.pack $ r of
                     Left  _ -> Nothing
                     Right v -> Just v
           ) =<< region'

readConfFile :: String -> FilePath -> IO [(String, String)]
readConfFile profile file = fmap (trim_ . breakOn "=") . ls <$> readFile file
  where
    ls   = takeWhile p1 . drop 1 . dropWhile p2 . lines
    p1 l = null l || head l /= '['
    p2 l = trimEnd l /= "[" ++ profile ++ "]"
    trim_ (a, b) = (trim a, trim . drop 1 $ b)

makeHashMapWith :: ListObjectsV2Response -> MyHashMap
makeHashMapWith res = let contents = res ^. lovrsContents
  in foldl (\h c -> h & at (c ^. oKey) ?~ defSC{scCloudETag = c ^. oETag})
     H.empty contents


